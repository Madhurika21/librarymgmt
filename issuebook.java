
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class issuebook extends javax.swing.JFrame {

    public issuebook() {
        initComponents();
        
    }
   

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        isbn_no = new javax.swing.JTextField();
        sid = new javax.swing.JTextField();
        sname = new javax.swing.JTextField();
        scontact = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(5000, 5000));
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Freestyle Script", 0, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Issue Book");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(159, -5, 160, 100);

        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("ISBN NO:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(20, 95, 110, 30);

        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("Student Id:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(20, 141, 110, 14);

        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Student Name:");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(20, 187, 110, 14);

        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Student Contact:");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(20, 227, 110, 14);
        getContentPane().add(isbn_no);
        isbn_no.setBounds(134, 102, 173, 20);

        sid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sidActionPerformed(evt);
            }
        });
        getContentPane().add(sid);
        sid.setBounds(134, 138, 173, 20);
        getContentPane().add(sname);
        sname.setBounds(134, 184, 173, 20);
        getContentPane().add(scontact);
        scontact.setBounds(134, 224, 173, 20);

        jButton1.setForeground(new java.awt.Color(255, 51, 51));
        jButton1.setText("Issue Book");
        jButton1.setBorder(new javax.swing.border.MatteBorder(null));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(30, 310, 90, 40);

        jButton2.setForeground(new java.awt.Color(0, 204, 102));
        jButton2.setText("Back");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(241, 309, 55, 23);

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/old-letters-436503__340.jpg"))); // NOI18N
        getContentPane().add(jLabel6);
        jLabel6.setBounds(0, -20, 500, 100);

        jButton3.setForeground(new java.awt.Color(255, 51, 51));
        jButton3.setText("clear");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3);
        jButton3.setBounds(223, 340, 70, 23);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 90, 500, 280);

        jPanel2.setBackground(new java.awt.Color(102, 51, 0));
        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 70, 500, 20);

        pack();
    }// </editor-fold>//GEN-END:initComponents
/**/
    private void sidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sidActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sidActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
 try{
      Class.forName("com.mysql.jdbc.Driver");
           Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb","root","root");  
            String query="INSERT INTO issuebooks(isbn_no,sid,sname,scontact)values(?,?,?,?)";
            String query1="update books set quantity=quantity-1,issued=issued+1 where isbno=?";
            
            PreparedStatement ps=conn.prepareStatement(query); 
             PreparedStatement pst=conn.prepareStatement(query1); 
         //   PreparedStatement pstt=conn.prepareStatement(query2); 
    //conn.prepareStatement(query1);
     if (isbn_no.getText().length()==0)
     JOptionPane.showMessageDialog(null,"Empty Fields Detected, Please fill up all fields!!!");
else if (sid.getText().length()==0)
      JOptionPane.showMessageDialog(null,"Empty Fields Detected, Please fill up all fields!!!");
  else if (sname.getText().length()==0)
      JOptionPane.showMessageDialog(null,"Empty Fields Detected, Please fill up all fields!!!");  
  else if (scontact.getText().length()==0)
      JOptionPane.showMessageDialog(null,"Empty Fields Detected, Please fill up all fields!!!");
 
else{
            ps.setInt(1,Integer.parseInt(isbn_no.getText()));
            
             ps.setInt(2,Integer.parseInt(sid.getText()));
            
              ps.setString(3,sname.getText()); 
              
             ps.setInt(4,Integer.parseInt(scontact.getText()));
             
             ps.executeUpdate();
             
               pst.setInt(1,Integer.parseInt(isbn_no.getText()));
            /* pst.setInt(2,Integer.parseInt(sid.getText()));
              pst.setString(3,sname.getText()); 
             pst.setInt(4,Integer.parseInt(scontact.getText()));*/
             pst.executeUpdate();
           
               JOptionPane.showMessageDialog(null,"Book issued succesfully");
               new viewissuebk().setVisible(true);
               dispose();
               conn.close();
             //  new viewissuebk().setVisible(true);
               
          //  PreparedStatement ps=conn.executeQuery(query); 
                   
  }            
               
    }                                        
   catch(ClassNotFoundException | SQLException e)
               {
                    JOptionPane.showMessageDialog(null,e);
                    
               }
 
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
new librarysection().setVisible(true); 
dispose();// TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
     isbn_no.setText("");
     sid.setText("");
     sname.setText("");
scontact.setText("");// TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed



    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(issuebook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(issuebook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(issuebook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(issuebook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new issuebook().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTextField isbn_no;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JTextField scontact;
    public javax.swing.JTextField sid;
    public javax.swing.JTextField sname;
    // End of variables declaration//GEN-END:variables

}
